import React, {Component} from 'react';

class TableRow extends Component {
  constructor(props) {
    super(props);
    this.deleteButtonClickHandler = this.deleteButtonClickHandler.bind(this);
    this.editButtonClickHandler = this.editButtonClickHandler.bind(this);
  }
  deleteButtonClickHandler(event) {
    event.preventDefault();
    this.props.deleteMethod(this.props.obj.id);
  }
  editButtonClickHandler(event) {
    event.preventDefault();
    this.props.editMethod(this.props.obj.id);
  }
  render() {
    return (
      <tr>
        <td>
          {this.props.obj.id}
        </td>
        <td>
          {this.props.obj.name}
        </td>
        <td>
          {this.props.obj.price}
        </td>
        <td>
          <a href="#" onClick={this.editButtonClickHandler}>Edit</a>&nbsp;|&nbsp;<a href="#" onClick={this.deleteButtonClickHandler}>Delete</a>
        </td>
      </tr>
    );
  }
}

export default TableRow;
