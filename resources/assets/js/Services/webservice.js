import axios from 'axios';

const BASE_URL = 'http://localhost:8000';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

export function insert(data) {
  let url = BASE_URL+'/items';
  return axios.post(url,data).then((response) => {
    return response;
  });
}

export function getitems() {
  let url = BASE_URL+'/items';
  return axios.get(url).then((response) => {
    return response.data;
  });
}

export function deleteItem(data) {
  let url = BASE_URL+'/items/'+data;
  return axios.delete(url).then((response) => {
    return response;
  });
}

export function getEditItem(data) {
  let url = BASE_URL+'/items/'+data+'/edit';
  return axios.get(url).then((response) => {
    return response;
  });
}

export function updateItem(data) {
  let url = BASE_URL+'/items/'+data.id;
  return axios.put(url, data).then((response) => {
    return response;
  });
}
