import React, {Component} from 'react';
import {insert} from '../Services/webservice';
import {withRouter} from 'react-router-dom';

class CreateItem extends Component {
    constructor(props) {
      super(props);
      this.state = {
        name : '',
        price : ''
      }
      this.textInputChangeEvent = this.textInputChangeEvent.bind(this);
      this.submitForm = this.submitForm.bind(this);
    }
    textInputChangeEvent(event) {
      if(event.target.id == 'name') {
        this.setState({name : event.target.value});
      } else {
        this.setState({price : event.target.value});
      }
    }
    submitForm(event) {
      event.preventDefault();
      insert(this.state);
      this.props.history.push('/');
    }
    render() {
      return (
      <div className="container">
        <h1>Create An Item</h1>
        <form>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>Item Name:</label>
                <input type="text" className="form-control" id="name" onChange={this.textInputChangeEvent}/>
              </div>
            </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Item Price:</label>
                  <input type="text" className="form-control" id="price" onChange={this.textInputChangeEvent}/>
                </div>
              </div>
            </div><br />
            <div className="form-group">
              <button className="btn btn-primary" onClick={this.submitForm}>Add Item</button>
            </div>
        </form>
  </div>
      )
    }
}
export default withRouter(CreateItem);
