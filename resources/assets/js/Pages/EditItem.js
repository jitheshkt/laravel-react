import React, {Component} from 'react';
import {getEditItem,updateItem} from '../Services/webservice';
import {withRouter} from 'react-router-dom';

class EditItem extends Component {
    constructor(props) {
      super(props);
      this.state = {
        'name' : '',
        'price' : '',
        id : ''
      }
      this.textInputChangeEvent = this.textInputChangeEvent.bind(this);
      this.updateItemEvent = this.updateItemEvent.bind(this);
    }
    textInputChangeEvent(event) {
      if(event.target.id == 'name') {
        this.setState({name : event.target.value});
      } else {
        this.setState({price : event.target.value});
      }
    }
    updateItemEvent(event) {
      event.preventDefault();
      updateItem({'name': this.state.name, 'id' : this.state.id, 'price' : this.state.price}).then((response) => {
        this.props.history.push('/');
      })
    }
    componentDidMount() {
      getEditItem(this.props.match.params.id).then((response) => {
        this.setState({name : response.data.name, price: response.data.price, id: response.data.id});
      });
    }
    render() {
      return (
      <div className="container">
        <h1>Edit : {this.props.match.params.id}</h1>
        <form>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>Item Name:</label>
                <input type="text" className="form-control" id="name" onChange={this.textInputChangeEvent} value={this.state.name}/>
              </div>
            </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>Item Price:</label>
                  <input type="text" className="form-control" id="price" onChange={this.textInputChangeEvent} value={this.state.price}/>
                </div>
              </div>
            </div><br />
            <div className="form-group">
              <button className="btn btn-primary" onClick={this.updateItemEvent}>Update Item</button>
            </div>
        </form>
  </div>
      )
    }
}
export default withRouter(EditItem);
