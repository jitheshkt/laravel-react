import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import TableRow from '../Components/TableRow';
import {getitems, deleteItem} from '../Services/webservice';

class Home extends Component {
    constructor(props) {
      super(props);
      this.state = {
        items :''
      }
      this.deleteRow  = this.deleteRow.bind(this);
      this.loadData = this.loadData.bind(this);
      this.editRow = this.editRow.bind(this);
    }
    componentDidMount() {
      this.loadData();
    }
    loadData() {
      var that = this; //this that hack
      getitems().then(function(data){
        that.setState({items: data});
      });
    }
    showRows() {
      var that = this;
      if(this.state.items instanceof Array) {
        return this.state.items.map(function(object, i){
          return <TableRow obj={object} key={i} deleteMethod={that.deleteRow} editMethod={that.editRow}/>
        })
      }
    }
    deleteRow(deleteid) {
      var that = this;
      deleteItem(deleteid).then(function(data){
        that.loadData();
      });
    }
    editRow(recordid) {
      this.props.history.push('/edit/'+recordid);
    }
    render() {
        return (
          <div className="container">
            <h1>All Items</h1>
            <div className="row">
              <div className="col-md-10"></div>
              <div className="col-md-2">
                <Link to="/new" className="btn btn-primary">Create Item</Link>
              </div>
            </div> <br />
            <table className="table table-hover">
              <thead>
                <tr>
                  <td>ID</td>
                  <td>Item Name</td>
                  <td>Item Price</td>
                  <td>Actions</td>
                </tr>
              </thead>
              <tbody>
                {this.showRows()}
              </tbody>
            </table>
          </div>
        );
    }
}

export default Home;
