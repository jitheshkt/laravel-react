import React from 'react';
import ReactDOM from 'react-dom';
import {
Route,
Switch,
HashRouter
} from 'react-router-dom';
import Home from './Pages/Home';
import CreateItem from './Pages/CreateItem';
import EditItem from './Pages/EditItem';

ReactDOM.render((
<HashRouter>
    <Switch>
      <Route exact path='/' component={Home}/>
        <Route path='/new' component={CreateItem}/>
        <Route path='/edit/:id' component={EditItem}/>
    </Switch>
</HashRouter>
), document.getElementById('root'));
